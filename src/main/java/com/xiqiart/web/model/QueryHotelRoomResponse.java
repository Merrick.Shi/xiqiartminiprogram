package com.xiqiart.web.model;

import com.xiqiart.entity.Course;
import com.xiqiart.entity.HotelRoom;

import java.util.List;

public class QueryHotelRoomResponse {
    private List<HotelRoom> hotelRooms;
    private int pageIndex;
    private int totalCount;

    public List<HotelRoom> getHotelRooms() {
        return hotelRooms;
    }

    public void setHotelRooms(List<HotelRoom> hotelRooms) {
        this.hotelRooms = hotelRooms;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
