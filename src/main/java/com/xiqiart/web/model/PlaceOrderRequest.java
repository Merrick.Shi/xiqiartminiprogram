package com.xiqiart.web.model;

import java.util.Date;
import java.util.List;

public class PlaceOrderRequest {
    private String userId;
    private String courseId;
    private String courseName;
    private String orgName;
    private int attendanceCount;
    private String contactName;
    private String contactPhone;
    private int courseCount;
    private Date arriveDate;
    private int dayCount;
    private float courseAmount;
    private float amount;
    private String orderComment;
    private List<PlaceOrderRoomModel> rooms;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public int getAttendanceCount() {
        return attendanceCount;
    }

    public void setAttendanceCount(int attendanceCount) {
        this.attendanceCount = attendanceCount;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public int getCourseCount() {
        return courseCount;
    }

    public void setCourseCount(int courseCount) {
        this.courseCount = courseCount;
    }

    public Date getArriveDate() {
        return arriveDate;
    }

    public void setArriveDate(Date arriveDate) {
        this.arriveDate = arriveDate;
    }

    public int getDayCount() {
        return dayCount;
    }

    public void setDayCount(int dayCount) {
        this.dayCount = dayCount;
    }

    public float getCourseAmount() {
        return courseAmount;
    }

    public void setCourseAmount(float courseAmount) {
        this.courseAmount = courseAmount;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public List<PlaceOrderRoomModel> getRooms() {
        return rooms;
    }

    public void setRooms(List<PlaceOrderRoomModel> rooms) {
        this.rooms = rooms;
    }

    public String getOrderComment() {
        return orderComment;
    }

    public void setOrderComment(String orderComment) {
        this.orderComment = orderComment;
    }
}

