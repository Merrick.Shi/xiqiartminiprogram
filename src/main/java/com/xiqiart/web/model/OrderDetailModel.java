package com.xiqiart.web.model;

import com.xiqiart.entity.Order;
import com.xiqiart.entity.OrderRoom;

import java.util.List;

public class OrderDetailModel {
    private Order order;
    private List<OrderRoom> orderRooms;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public List<OrderRoom> getOrderRooms() {
        return orderRooms;
    }

    public void setOrderRooms(List<OrderRoom> orderRooms) {
        this.orderRooms = orderRooms;
    }
}
