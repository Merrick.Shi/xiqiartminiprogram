package com.xiqiart.web.model;

import com.xiqiart.entity.Order;

import java.util.List;

public class QueryOrderResponse {
    private List<Order> orders;
    private int pageIndex;
    private int totalCount;

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
