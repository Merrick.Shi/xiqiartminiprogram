package com.xiqiart.web.model;

import com.xiqiart.entity.Course;

import java.util.List;

public class QueryCourseResponse {
    private List<Course> courses;
    private int pageIndex;
    private int totalCount;

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
