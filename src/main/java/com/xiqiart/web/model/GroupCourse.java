package com.xiqiart.web.model;

import com.xiqiart.entity.Course;

import java.util.List;

public class GroupCourse {
    private String groupName;
    private Integer groupId;
    private List<Course> courses;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}
