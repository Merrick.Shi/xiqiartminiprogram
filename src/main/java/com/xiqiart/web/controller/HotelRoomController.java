package com.xiqiart.web.controller;

import com.xiqiart.common.JSONResult;
import com.xiqiart.service.HotelRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HotelRoomController {

	@Autowired
	private HotelRoomService hotelRoomService;
	

	@GetMapping("/api/hotelroomquery")
	public JSONResult hotelRoomQuery() {

		return JSONResult.ok(hotelRoomService.queryHotelRooms());
	}
	
}
