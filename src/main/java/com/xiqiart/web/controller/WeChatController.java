package com.xiqiart.web.controller;

import com.xiqiart.common.JSONResult;
import com.xiqiart.service.UserService;
import com.xiqiart.web.model.WeChatLoginRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeChatController {

	@Autowired
	private UserService userService;
	

	@PostMapping("/api/wechatlogin")
	public JSONResult wechatLogin(@RequestBody WeChatLoginRequest request) {

		return JSONResult.ok(userService.wechatLogin(request));
	}
	
}
