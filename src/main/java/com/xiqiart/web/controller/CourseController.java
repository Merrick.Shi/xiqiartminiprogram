package com.xiqiart.web.controller;

import com.xiqiart.common.JSONResult;
import com.xiqiart.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CourseController {

	@Autowired
	private CourseService courseService;
	

	@GetMapping("/api/coursequery")
	public JSONResult courseQuery() {

		return JSONResult.ok(courseService.queryCourses());
	}
	
}
