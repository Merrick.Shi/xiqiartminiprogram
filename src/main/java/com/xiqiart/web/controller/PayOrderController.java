package com.xiqiart.web.controller;

import com.xiqiart.common.JSONResult;
import com.xiqiart.service.PayOrderService;
import com.xiqiart.web.model.NotifyOrderPaidRequest;
import com.xiqiart.web.model.PayOrderRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class PayOrderController {

	@Autowired
	private PayOrderService payOrderService;

	@PostMapping("/api/payorder")
	public String payOrder(@RequestBody PayOrderRequest payOrderRequest, HttpServletRequest request){
		return payOrderService.payOrder(payOrderRequest, request);
	}

	@PostMapping("/api/orderpaid")
	public JSONResult notifyOrderPaid(@RequestBody NotifyOrderPaidRequest request){
		int result =payOrderService.notifyOrderPaid(request);
		if(result > 0){

			return JSONResult.ok();
		}else{
			return  JSONResult.errorMsg("failed to notify");
		}
	}
}
