package com.xiqiart.web.controller;

import com.xiqiart.entity.Course;
import com.xiqiart.entity.HotelRoom;
import com.xiqiart.entity.Order;
import com.xiqiart.service.CourseService;
import com.xiqiart.service.HotelRoomService;
import com.xiqiart.service.OrderService;
import com.xiqiart.util.ExcelData;
import com.xiqiart.util.ExportExcelUtils;
import com.xiqiart.web.WebSecurityConfig;
import com.xiqiart.web.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.UrlResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


/**
 * Created by huangds on 2017/10/24.
 */
@Controller
public class AdminController {


    private static String UPLOADED_FOLDER = "./upload/";

    @Value("${site_address}")
    private String siteAddress;

    @Autowired
    private OrderService orderService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private HotelRoomService hotelRoomService;


    @GetMapping("/")
    public String index(@SessionAttribute(WebSecurityConfig.SESSION_KEY) String account, Model model) {

        return "index";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @PostMapping("/loginVerify")
    public String loginVerify(String username, String password, HttpSession session) {
        Admin admin = new Admin();
        admin.setUsername(username);
        admin.setPassword(password);

        boolean verify = false;

        if (username.equals("Admin") && password.equals("Admin123")) {
            verify = true;
        }

        if (verify) {
            session.setAttribute(WebSecurityConfig.SESSION_KEY, username);
            return "redirect:/orderlist";
        } else {
            return "redirect:/login";
        }
    }

    @GetMapping("/logout")
    public String logout(HttpSession session) {
        session.removeAttribute(WebSecurityConfig.SESSION_KEY);
        return "redirect:/login";
    }

    @GetMapping("/orderlist")
    public String orderlist(Model model,
                            @RequestParam(required = false, defaultValue = "") String contactPhone, @RequestParam(required = false, defaultValue = "") String orderNo,
                            @RequestParam(required = false, defaultValue = "1") int pageIndex, @RequestParam(required = false, defaultValue = "20") int pageSize) {

        QueryOrderResponse response = orderService.queryOrderList(contactPhone, orderNo, pageIndex, pageSize);

        int totalPage = 1;
        int totalCount = response.getTotalCount();
        if (totalCount % pageSize == 0) {
            totalPage = totalCount / pageSize;
        } else {
            totalPage = totalCount / pageSize + 1;
        }

        model.addAttribute("orders", response.getOrders());
        model.addAttribute("indexPage", pageIndex);
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("contactPhone", contactPhone);
        model.addAttribute("orderNo", orderNo);
        return "orderlist";
    }

    @GetMapping("/demo")
    public String demo(Model model, @RequestParam int page) {

        model.addAttribute("indexPage", page);
        model.addAttribute("totalPage", 10);
        return "demo";
    }

    @ResponseBody
    @PostMapping("/checkinorder")
    public String checkinOrder(@RequestParam String orderNo) {
        orderService.checkInOrder(orderNo);
        return "success";
    }

    @GetMapping("/orderdetail/{orderNo}")
    public String orderdetail(Model model, @PathVariable String orderNo) {
        OrderFullInfo info = orderService.queryOrderFullInfo(orderNo);
        model.addAttribute("order", info.getOrder());
        model.addAttribute("orderRooms", info.getOrderRooms());
        return "orderdetail";
    }


    @GetMapping("/courselist")
    public String courselist(Model model,
                             @RequestParam(required = false, defaultValue = "1") int pageIndex, @RequestParam(required = false, defaultValue = "20") int pageSize) {

        QueryCourseResponse response = courseService.queryCourseList(pageIndex, pageSize);
        int totalPage = 1;
        int totalCount = response.getTotalCount();
        if (totalCount % pageSize == 0) {
            totalPage = totalCount / pageSize;
        } else {
            totalPage = totalCount / pageSize + 1;
        }
        model.addAttribute("courses", response.getCourses());
        model.addAttribute("indexPage", pageIndex);
        model.addAttribute("totalPage", totalPage);
        return "courselist";
    }

    @GetMapping("/coursedetail/{courseId}")
    public String coursedetail(Model model, @PathVariable String courseId) {
        Course course = courseService.queryCourseByCourseId(courseId);
        if (course == null) {
            course = new Course();
        }
        model.addAttribute("course", course);
        return "coursedetail";
    }

    @GetMapping("/coursedelete/{courseId}")
    public String coursedelete(Model model, @PathVariable String courseId) {
        courseService.deleteCourse(courseId);
        return "redirect:/courselist";
    }

    @PostMapping("/savecourse")
    public String saveCourse(@RequestParam("file") MultipartFile file, CoursePostRequest request, Model model,
                             RedirectAttributes redirectAttributes) {

        boolean isNew = false;

        Course course = courseService.queryCourseByCourseId(request.getCourseId());
        if (file.isEmpty() && course == null) {
            model.addAttribute("course", course);
            return "coursedetail";
        } else {
            if (course == null) {
                isNew = true;
                course = new Course();
                course.setCourseId(UUID.randomUUID().toString());
                course.setCreateTime(new Date());
            }

            if (!file.isEmpty()) {

                try {

                    byte[] bytes = file.getBytes();

                    Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());

                    Path pathDir = Paths.get(UPLOADED_FOLDER);
                    if (!Files.exists(pathDir))
                        Files.createDirectories(pathDir);

                    Files.write(path, bytes);

                    course.setCourseUrl(siteAddress + "files/" + file.getOriginalFilename());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            course.setLastEditTime(new Date());
            course.setCourseName(request.getCourseName());
            course.setMonth(request.getMonth());
            course.setYear(request.getYear());
            course.setPriceOrg(request.getPricePerson());
            course.setPricePerson(request.getPricePerson());

            if (isNew) {
                courseService.saveCourse(course);
            } else {
                courseService.updateCourse(course);
            }

        }

        return "redirect:/courselist";
    }

    @RequestMapping(value = "/files/{filename:.+}", method = RequestMethod.GET, produces = "image/png")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Path path = Paths.get(UPLOADED_FOLDER);
        Path file = path.resolve(filename);

        Resource resource = null;

        try {
            resource = new UrlResource(file.toUri());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


        try {
            return ResponseEntity.ok(resource);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }


//        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
//                "attachment; filename=\"" + resource.getFilename() + "\"").body(resource);
    }

    @GetMapping("/test")
    public String index() {
        return "upload";
    }

    @PostMapping("/upload") // //new annotation since 4.3
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:uploadStatus";
        }

        try {

            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();

            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());

            Path parentDir = path.getParent();
            if (!Files.exists(parentDir))
                Files.createDirectories(parentDir);

            Files.write(path, bytes);
            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/uploadStatus";
    }


    @GetMapping("/hotelroomlist")
    public String hotelroomlist(Model model,
                                @RequestParam(required = false, defaultValue = "1") int pageIndex, @RequestParam(required = false, defaultValue = "20") int pageSize) {

        QueryHotelRoomResponse response = hotelRoomService.queryHotelRoomList(pageIndex, pageSize);
        int totalPage = 1;
        int totalCount = response.getTotalCount();
        if (totalCount % pageSize == 0) {
            totalPage = totalCount / pageSize;
        } else {
            totalPage = totalCount / pageSize + 1;
        }
        model.addAttribute("hotelRooms", response.getHotelRooms());
        model.addAttribute("indexPage", pageIndex);
        model.addAttribute("totalPage", totalPage);
        return "hotelroomlist";
    }

    @GetMapping("/hotelroomdetail/{hotelRoomId}")
    public String hotelroomdetail(Model model, @PathVariable String hotelRoomId) {
        HotelRoom hotelRoom = hotelRoomService.queryHotelRoomById(hotelRoomId);
        if (hotelRoom == null) {
            hotelRoom = new HotelRoom();
        }
        model.addAttribute("hotelroom", hotelRoom);
        return "hotelroomdetail";
    }

    @PostMapping("/savehotelroom")
    public String savehotelroom(HotelRoomPostRequest request, Model model,
                                RedirectAttributes redirectAttributes) {
        boolean isNew = false;
        HotelRoom hotelRoom = hotelRoomService.queryHotelRoomById(request.getRoomId());
        if (hotelRoom == null) {
            isNew = true;
            hotelRoom = new HotelRoom();
            hotelRoom.setRoomId(UUID.randomUUID().toString());
            hotelRoom.setCreateTime(new Date());
        }

        hotelRoom.setLastEditTime(new Date());
        hotelRoom.setAmount(request.getAmount());
        hotelRoom.setRoomTypeName(request.getRoomTypeName());
        hotelRoom.setPersonHold(1);

        if (isNew) {
            hotelRoomService.saveHotelRoom(hotelRoom);
        } else {
            hotelRoomService.updateHotelRoom(hotelRoom);
        }
        return "redirect:/hotelroomlist";
    }

    @PostMapping("/export")
    public void export(HttpServletResponse response) throws Exception {


        List<OrderRoomInfo> orders = orderService.queryAllOrders();

        ExcelData data = new ExcelData();
        data.setName("订单汇总");
        List<String> titles = new ArrayList();
        titles.add("订单号");
        titles.add("订单状态");
        titles.add("课程名称");
        titles.add("下单时间");
        titles.add("入住时间");
        titles.add("订单金额");
        titles.add("联系人");
        titles.add("联系电话");
        titles.add("入住天数");
        titles.add("房间类型");
        titles.add("入住人姓名");
        data.setTitles(titles);

        List<List<Object>> rows = new ArrayList();
        for(OrderRoomInfo order : orders){
            List<Object> row = new ArrayList();
            row.add(order.getOrderNo());
            row.add(order.getOrderStatusString());
            row.add(order.getCourseName());
            row.add(order.getCreateTimeString());
            row.add(order.getArriveDateString());
            row.add(order.getAmount());
            row.add(order.getContactName());
            row.add(order.getContactPhone());
            row.add(order.getDayCount());
            row.add(order.getRoomTypeName());
            row.add(order.getGuestName());
            rows.add(row);
        }

        data.setRows(rows);
        ExportExcelUtils.exportExcel(response, "订单汇总.xlsx", data);
    }
}
