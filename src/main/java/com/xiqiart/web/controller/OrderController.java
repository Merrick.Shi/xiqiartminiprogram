package com.xiqiart.web.controller;

import com.xiqiart.common.JSONResult;
import com.xiqiart.service.OrderService;
import com.xiqiart.web.model.PlaceOrderRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController {

	@Autowired
	private OrderService orderService;
	

	@GetMapping("/api/userorderquery")
	public JSONResult userOrderQuery(@RequestParam String userId) {
		return JSONResult.ok(orderService.queryUserOrders(userId));
	}

	@GetMapping("/api/queryorder")
	public JSONResult orderQuery(@RequestParam String userId, @RequestParam String orderNo) {
		return JSONResult.ok(orderService.orderQuery(userId, orderNo));
	}

	@Transactional
	@PostMapping("/api/placeorder")
	public JSONResult placeOrder(@RequestBody PlaceOrderRequest request) {
		String orderNo = orderService.placeOrder(request);
		if(orderNo != ""){
			return JSONResult.ok(orderNo);
		}else{
			return  JSONResult.errorException("error occured");
		}

	}

	@GetMapping("/api/paidorders")
	public JSONResult paidOrderQuery(@RequestParam String userId) {
		return JSONResult.ok(orderService.orderTabQuery(userId, 2));
	}

	@GetMapping("/api/finishedorders")
	public JSONResult finishedOrderQuery(@RequestParam String userId) {
		return JSONResult.ok(orderService.orderTabQuery(userId, 3));
	}
}
