package com.xiqiart.web.controller;

import com.xiqiart.common.JSONResult;
import com.xiqiart.entity.User;
import com.xiqiart.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(value="/user",method = RequestMethod.GET)
    public Map<String,Object> queryUser(){
        Map<String,Object> modelMap = new HashMap<>();
        List<User> list = userService.queryUser();
        modelMap.put("userList",list);
        return modelMap;
    }

    @RequestMapping(value="/user",method = RequestMethod.PUT)
    public Map<String,Object> insertUser(@RequestBody User user){
        Map<String,Object> modelMap = new HashMap<>();
        boolean result = userService.insertUser(user);
        modelMap.put("success",result);
        return modelMap;
    }

    @RequestMapping(value="/user",method = RequestMethod.PATCH)
    public Map<String,Object> updateUser(@RequestBody User user){
        Map<String,Object> modelMap = new HashMap<>();
        boolean result = userService.updateUser(user);
        modelMap.put("success",result);
        return modelMap;
    }

    @RequestMapping(value="/user/{id}",method = RequestMethod.DELETE)
    public Map<String,Object> deleteUser(@PathVariable("id") Integer userId){
        Map<String,Object> modelMap = new HashMap<>();
        boolean result = userService.deleteUser(userId);
        modelMap.put("success",result);
        return modelMap;
    }


    @GetMapping("/api/queryuserbyid")
    public JSONResult getUserInfo(@RequestParam String userId){
        return JSONResult.ok(userService.queryUserByUserId(userId));
    }
}

