package com.xiqiart.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Course {
    private Integer id;
    private String courseId;
    private String courseName;
    private String courseUrl;
    private float priceOrg;
    private float pricePerson;
    private int month;
    private int year;
    //创建时间
    private Date createTime;
    //更新时间
    private Date lastEditTime;

    private String createTimeString;

    public String getCreateTimeString() {
        if(createTime == null){
            return "";
        }else{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.format(createTime);
        }
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseUrl() {
        return courseUrl;
    }

    public void setCourseUrl(String courseUrl) {
        this.courseUrl = courseUrl;
    }

    public float getPriceOrg() {
        return priceOrg;
    }

    public void setPriceOrg(float priceOrg) {
        this.priceOrg = priceOrg;
    }

    public float getPricePerson() {
        return pricePerson;
    }

    public void setPricePerson(float pricePerson) {
        this.pricePerson = pricePerson;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastEditTime() {
        return lastEditTime;
    }

    public void setLastEditTime(Date lastEditTime) {
        this.lastEditTime = lastEditTime;
    }
}
