package com.xiqiart.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HotelRoom {

    private int id;
    private String roomId;
    private String roomTypeName;
    private float amount;
    private int personHold;
    //创建时间
    private Date createTime;
    //更新时间
    private Date lastEditTime;

    private String createTimeString;

    public String getCreateTimeString() {
        if(createTime == null){
            return "";
        }else{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.format(createTime);
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomTypeName() {
        return roomTypeName;
    }

    public void setRoomTypeName(String roomTypeName) {
        this.roomTypeName = roomTypeName;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public int getPersonHold() {
        return personHold;
    }

    public void setPersonHold(int personHold) {
        this.personHold = personHold;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastEditTime() {
        return lastEditTime;
    }

    public void setLastEditTime(Date lastEditTime) {
        this.lastEditTime = lastEditTime;
    }
}
