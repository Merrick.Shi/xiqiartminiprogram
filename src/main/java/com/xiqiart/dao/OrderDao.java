package com.xiqiart.dao;

import com.xiqiart.entity.Order;
import com.xiqiart.entity.OrderRoom;
import com.xiqiart.web.model.OrderRoomInfo;
import com.xiqiart.web.model.QueryOrderResponse;

import java.util.List;

public interface OrderDao {

    List<Order> queryUserOrders(String userId);
    int addOrder(Order order);
    void addOrderRoom(OrderRoom orderRoom);
    Order orderQuery(String userId, String orderNo);
    List<OrderRoom> orderRoomQuery(String orderNo);
    int updateOrder(Order order);
    List<Order> orderTabQuery(String userId, int orderStatus);

    List<Order> queryOrderList(String contactPhone, String orderNo, int offset, int pageSize);
    int queryOrderListCount(String contactPhone, String orderNo);

    Order querySingleOrder(String orderNo);

    void adminUpdateOrder(Order order);

    List<OrderRoomInfo> queryAllOrders();
}
