package com.xiqiart.dao;

import com.xiqiart.entity.User;

import java.util.List;

public interface UserDao {

    List<User> queryUser();

    User queryUserByUserId(String userId);

    int insertUser(User user);

    int updateUser(User user);


    int deleteUser(Integer userId);

    User queryUserByWechatOpenId(String wechatOpenId);
}
