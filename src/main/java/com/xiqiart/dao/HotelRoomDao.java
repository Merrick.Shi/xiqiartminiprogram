package com.xiqiart.dao;

import com.xiqiart.entity.HotelRoom;

import java.util.List;

public interface HotelRoomDao {

    List<HotelRoom> queryHotelRooms();

    HotelRoom queryHotelRoomById(String roomId);

    List<HotelRoom> queryHotelRoomList(int offset, int pageSize);

    int queryHotelRoomListCount();

    void saveHotelRoom(HotelRoom hotelRoom);

    void updateHotelRoom(HotelRoom hotelRoom);
}
