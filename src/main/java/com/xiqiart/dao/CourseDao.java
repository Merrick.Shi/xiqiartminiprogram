package com.xiqiart.dao;

import com.xiqiart.entity.Course;

import java.util.List;

public interface CourseDao {

    List<Course> queryCourses();

    Course queryCourseByCourseId(String courseId);

    void updateCourse(Course course);

    void saveCourse(Course course);

    List<Course> queryCourseList(int offset, int pageSize);

    int queryCourseListCount();

    void deleteCourse(String courseId);
}
