package com.xiqiart.service;

import com.xiqiart.web.model.NotifyOrderPaidRequest;
import com.xiqiart.web.model.PayOrderRequest;

import javax.servlet.http.HttpServletRequest;

public interface PayOrderService {

    String payOrder(PayOrderRequest payOrderRequest, HttpServletRequest request);

    int notifyOrderPaid(NotifyOrderPaidRequest request);
}