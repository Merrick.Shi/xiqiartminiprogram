package com.xiqiart.service.impl;

import com.xiqiart.dao.HotelRoomDao;
import com.xiqiart.entity.HotelRoom;
import com.xiqiart.service.HotelRoomService;
import com.xiqiart.web.model.QueryHotelRoomResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class HotelRoomServiceImpl implements HotelRoomService {

    @Autowired
    private HotelRoomDao hotelRoomDao;

    @Override
    public List<HotelRoom> queryHotelRooms() {
        return hotelRoomDao.queryHotelRooms();
    }

    @Override
    public HotelRoom queryHotelRoomById(String roomId) { return hotelRoomDao.queryHotelRoomById(roomId); }

    @Override
    public QueryHotelRoomResponse queryHotelRoomList(int pageIndex, int pageSize) {

        QueryHotelRoomResponse response = new QueryHotelRoomResponse();
        int offset = (pageIndex - 1) * pageSize;
        response.setPageIndex(pageIndex);
        response.setHotelRooms(hotelRoomDao.queryHotelRoomList(offset, pageSize));
        response.setTotalCount(hotelRoomDao.queryHotelRoomListCount());

        return response;
    }

    @Override
    public void saveHotelRoom(HotelRoom hotelRoom) {
        hotelRoomDao.saveHotelRoom(hotelRoom);
    }

    @Override
    public void updateHotelRoom(HotelRoom hotelRoom) {
        hotelRoomDao.updateHotelRoom(hotelRoom);
    }
}


