package com.xiqiart.service.impl;

import com.xiqiart.dao.CourseDao;
import com.xiqiart.entity.Course;
import com.xiqiart.service.CourseService;
import com.xiqiart.web.model.GroupCourse;
import com.xiqiart.web.model.QueryCourseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseDao courseDao;

    @Override
    public List<GroupCourse> queryCourses() {
        Calendar cal = Calendar.getInstance();
        int currentYear = cal.get(Calendar.YEAR);
        List<GroupCourse> groupCourses = new ArrayList<>();
        List<Course> courses = courseDao.queryCourses();
        for (int j = currentYear; j <= currentYear + 1; j++) {
            for (int i = 1; i <= 12; i++) {
                String groupName = i + "月课程";
                List<Course> monthCourse = new ArrayList<>();

                for (Course course : courses) {
                    if (course.getMonth() == i && course.getYear() == j) {
                        monthCourse.add(course);
                    }
                }

                if (!monthCourse.isEmpty()) {
                    String groupId = String.format("%d%d", j,i);
                    GroupCourse groupCourse = new GroupCourse();
                    groupCourse.setGroupId(Integer.parseInt(groupId));
                    groupCourse.setGroupName(groupName);
                    groupCourse.setCourses((monthCourse));
                    groupCourses.add(groupCourse);
                }
            }
        }

        return groupCourses;
    }

    @Override
    public Course queryCourseByCourseId(String courseId) {
        return courseDao.queryCourseByCourseId(courseId);
    }

    @Override
    public QueryCourseResponse queryCourseList(int pageIndex, int pageSize) {
        QueryCourseResponse response = new QueryCourseResponse();

        int offset = (pageIndex - 1) * pageSize;


        response.setPageIndex(pageIndex);

        response.setCourses(courseDao.queryCourseList(offset, pageSize));
        response.setTotalCount(courseDao.queryCourseListCount());

        return response;
    }

    @Override
    public void saveCourse(Course course) {
        courseDao.saveCourse(course);
    }

    @Override
    public void updateCourse(Course course) {
        courseDao.updateCourse(course);
    }

    @Override
    public void deleteCourse(String courseId) {
        courseDao.deleteCourse(courseId);
    }
}


