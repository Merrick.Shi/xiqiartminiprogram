package com.xiqiart.service.impl;

import com.xiqiart.common.HttpClientUtil;
import com.xiqiart.common.JsonUtils;
import com.xiqiart.common.RedisOperator;
import com.xiqiart.dao.UserDao;
import com.xiqiart.entity.User;
import com.xiqiart.service.UserService;
import com.xiqiart.web.model.WeChatLoginRequest;
import com.xiqiart.web.model.WeChatSessionModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RedisOperator redis;

    @Override
    public List<User> queryUser() {
        return userDao.queryUser();
    }

    @Override
    public User queryUserByUserId(String userId) {
        return userDao.queryUserByUserId(userId);
    }


    @Value("${app_id}")
    private String appId;

    @Value("${app_secret}")
    private String appSecret;

    @Override
    @Transactional
    public boolean insertUser(User user) {
        if(user.getUserName()!=null && !"".equals(user.getUserName())){
            Date time = new Date();
            user.setCreateTime(time);
            user.setLastEditTime(time);
            try {
                int row = userDao.insertUser(user);
                if(row>0){
                    return true;
                }else {
                    throw new RuntimeException("插入区域信息失败！");
                }
            }catch (Exception e){
                throw new RuntimeException("插入区域信息失败："+e.getMessage());
            }
        }else{
            throw new RuntimeException("区域信息不能为空！");
        }
    }

    @Override
    @Transactional
    public boolean updateUser(User user) {
        if(user.getUserId()!=null && user.getId()>0){
            user.setLastEditTime(new Date());
            try {
                int row = userDao.updateUser(user);
                if(row>0){
                    return true;
                }else {
                    throw new RuntimeException("更新区域信息失败！");
                }
            }catch (Exception e){
                throw new RuntimeException("更新区域信息失败："+e.getMessage());
            }
        }else{
            throw new RuntimeException("区域id不能为空！");
        }
    }

    @Override
    @Transactional
    public boolean deleteUser(Integer userId) {
        if(userId!=null && userId>0){
            try {
                int row = userDao.deleteUser(userId);
                if(row>0){
                    return true;
                }else {
                    throw new RuntimeException("删除区域信息失败！");
                }
            }catch (Exception e){
                throw new RuntimeException("删除区域信息失败："+e.getMessage());
            }
        }else{
            throw new RuntimeException("区域id不能为空！");
        }
    }


    @Override
    @Transactional
    public WeChatSessionModel wechatLogin(WeChatLoginRequest request) {
        System.out.println("wxlogin - code: " + request.getCode());

        String url = "https://api.weixin.qq.com/sns/jscode2session";
        Map<String, String> param = new HashMap<>();
        param.put("appid", appId);
        param.put("secret", appSecret);
        param.put("js_code", request.getCode());
        param.put("grant_type", "authorization_code");

        String wxResult = HttpClientUtil.doGet(url, param);

        WeChatSessionModel model = JsonUtils.jsonToPojo(wxResult, WeChatSessionModel.class);

        User user = userDao.queryUserByWechatOpenId(model.getOpenid());

        if(user != null){
            user.setUserName(request.getNickName());
            user.setAvatarUrl(request.getAvatarUrl());
            user.setLastEditTime(new Date());
            userDao.updateUser(user);
            model.setUserId(user.getUserId());
        }else{
            String userId = UUID.randomUUID().toString();
            user = new User();
            user.setCreateTime(new Date());
            user.setUserName(request.getNickName());
            user.setAvatarUrl(request.getAvatarUrl());
            user.setLastEditTime(new Date());
            user.setUserId(userId);
            user.setLastEditTime(new Date());
            user.setWechatOpenId(model.getOpenid());
            userDao.insertUser(user);
            model.setUserId(userId);
        }

        return model;
    }
}


