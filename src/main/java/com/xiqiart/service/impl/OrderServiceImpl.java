package com.xiqiart.service.impl;

import com.xiqiart.dao.OrderDao;
import com.xiqiart.entity.*;
import com.xiqiart.service.CourseService;
import com.xiqiart.service.HotelRoomService;
import com.xiqiart.service.OrderService;
import com.xiqiart.service.UserService;
import com.xiqiart.web.model.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.xiqiart.common.RandomUtils;

import java.util.*;

import com.xiqiart.common.*;

import javax.swing.*;

@Service
public class OrderServiceImpl implements OrderService {


    @Value("${shop_id}")
    private String shopId;

    @Value("${url_unified_order}")
    private String urlUnifiedOrder;

    @Value("${app_id}")
    private String appId;

    @Value("${app_key}")
    private String appKey;

    @Value("${time_format}")
    private String timeFormat;

    @Value("${url_notify}")
    private String urlNotify;


    @Autowired
    private OrderDao orderDao;

    @Autowired
    private UserService userService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private HotelRoomService hotelRoomService;

    @Override
    public List<Order> queryUserOrders(String userId) {
        return orderDao.queryUserOrders(userId);
    }

    @Override
    public OrderDetailModel orderQuery(String userId, String orderNo) {

        OrderDetailModel orderModel = new OrderDetailModel();
        Order order = orderDao.orderQuery(userId, orderNo);
        orderModel.setOrder(order);
        orderModel.setOrderRooms(orderDao.orderRoomQuery(order.getOrderNo()));

        return orderModel;
    }

    @Override
    public List<Order> orderTabQuery(String userId, int orderStatus) {
        return orderDao.orderTabQuery(userId, orderStatus);
    }

    @Override
    public QueryOrderResponse queryOrderList(String contactPhone, String orderNo, int pageIndex, int pageSize) {
        QueryOrderResponse response = new QueryOrderResponse();

        int offset = (pageIndex - 1) * pageSize;


        response.setPageIndex(pageIndex);
        response.setOrders(orderDao.queryOrderList(contactPhone, orderNo, offset, pageSize));
        response.setTotalCount(orderDao.queryOrderListCount(contactPhone, orderNo));
        return response;
    }

    @Override
    public OrderFullInfo queryOrderFullInfo(String orderNo) {
        OrderFullInfo info = new OrderFullInfo();
        info.setOrder(orderDao.querySingleOrder(orderNo));
        info.setOrderRooms(orderDao.orderRoomQuery(orderNo));
        return info;
    }

    @Override
    public void checkInOrder(String orderNo) {
        Order order = orderDao.querySingleOrder(orderNo);
        order.setOrderStatus(3);
        order.setLastEditTime(new Date());
        orderDao.adminUpdateOrder(order);
    }

    @Override
    public List<OrderRoomInfo> queryAllOrders() {
        return orderDao.queryAllOrders();
    }


    @Override
    public String placeOrder(PlaceOrderRequest request) {
        String orderNo = UUID.randomUUID().toString();

        float amount = 0;
        Course course = courseService.queryCourseByCourseId(request.getCourseId());
        if (course == null) {
            orderNo = "";
        } else {

            amount += course.getPricePerson();

            List<PlaceOrderRoomModel> rooms = request.getRooms();
            if (rooms != null && !rooms.isEmpty()) {
                for (PlaceOrderRoomModel roomModel : rooms) {
                    HotelRoom room = hotelRoomService.queryHotelRoomById(roomModel.getRoomId());
                    amount += room.getAmount() * request.getDayCount();
                }
            }

            Order order = new Order();
            order.setOrderNo(orderNo);
            order.setUserId(request.getUserId());
            order.setCourseId((request.getCourseId()));
            order.setCourseName(request.getCourseName());
            order.setCourseAmount(request.getCourseAmount());
            order.setOrgName(request.getOrgName());
            order.setAttendanceCount(request.getAttendanceCount());
            order.setContactName(request.getContactName());
            order.setContactPhone(request.getContactPhone());
            order.setCourseCount(request.getCourseCount());
            order.setArriveDate(request.getArriveDate());
            order.setDayCount(request.getDayCount());
            order.setOrderComment(request.getOrderComment());
            order.setAmount(amount);
            order.setOrderStatus(1);
            order.setCreateTime(new Date());
            order.setOrgName(request.getOrgName());
            order.setLastEditTime(new Date());

            orderDao.addOrder(order);

            if (rooms != null && !rooms.isEmpty()) {
                for (PlaceOrderRoomModel roomModel : rooms) {
                    OrderRoom orderRoom = new OrderRoom();
                    orderRoom.setRoomId(roomModel.getRoomId());
                    orderRoom.setRoomTypeName(roomModel.getRoomTypeName());
                    orderRoom.setAmount(roomModel.getAmount());
                    orderRoom.setGuestName(roomModel.getGuestName());
                    orderRoom.setOrderNo(orderNo);
                    orderRoom.setCreateTime(new Date());
                    orderRoom.setLastEditTime(new Date());
                    orderDao.addOrderRoom(orderRoom);
                }
            }
        }
        return orderNo;
    }


    private String unifiedOrder(String openId, String clientIP, String randomNonceStr) {

        try {

            String url = urlUnifiedOrder;

            PayInfo payInfo = createPayInfo(openId, clientIP, randomNonceStr);
            String md5 = getSign(payInfo);
            payInfo.setSign(md5);

            String xml = CommonUtil.payInfoToXML(payInfo);
            xml = xml.replace("__", "_").replace("<![CDATA[1]]>", "1");
            //xml = xml.replace("__", "_").replace("<![CDATA[", "").replace("]]>", "");

            StringBuffer buffer = HttpUtil.httpsRequest(url, "POST", xml);
            Map<String, String> result = CommonUtil.parseXml(buffer.toString());


            String return_code = result.get("return_code");
            if (StringUtils.isNotBlank(return_code) && return_code.equals("SUCCESS")) {

                String return_msg = result.get("return_msg");
                if (StringUtils.isNotBlank(return_msg) && !return_msg.equals("OK")) {
                    //log.error("统一下单错误！");
                    return "";
                }

                String prepay_Id = result.get("prepay_id");
                return prepay_Id;

            } else {
                return "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    private PayInfo createPayInfo(String openId, String clientIP, String randomNonceStr) {

        Date date = new Date();
        String timeStart = TimeUtils.getFormatTime(date, timeFormat);
        String timeExpire = TimeUtils.getFormatTime(TimeUtils.addDay(date, 2), timeFormat);

        String randomOrderId = CommonUtil.getRandomOrderId();

        PayInfo payInfo = new PayInfo();
        payInfo.setAppid(appId);
        payInfo.setMch_id(shopId);
        payInfo.setDevice_info("WEB");
        payInfo.setNonce_str(randomNonceStr);
        payInfo.setSign_type("MD5");  //默认即为MD5
        payInfo.setBody("JSAPI支付测试");
        payInfo.setAttach("支付测试4luluteam");
        payInfo.setOut_trade_no(randomOrderId);
        payInfo.setTotal_fee(1);
        payInfo.setSpbill_create_ip(clientIP);
        payInfo.setTime_start(timeStart);
        payInfo.setTime_expire(timeExpire);
        payInfo.setNotify_url(urlNotify);
        payInfo.setTrade_type("JSAPI");
        payInfo.setLimit_pay("no_credit");
        payInfo.setOpenid(openId);

        return payInfo;
    }

    private String getSign(PayInfo payInfo) throws Exception {
        StringBuffer sb = new StringBuffer();
        sb.append("appid=" + payInfo.getAppid())
                .append("&attach=" + payInfo.getAttach())
                .append("&body=" + payInfo.getBody())
                .append("&device_info=" + payInfo.getDevice_info())
                .append("&limit_pay=" + payInfo.getLimit_pay())
                .append("&mch_id=" + payInfo.getMch_id())
                .append("&nonce_str=" + payInfo.getNonce_str())
                .append("&notify_url=" + payInfo.getNotify_url())
                .append("&openid=" + payInfo.getOpenid())
                .append("&out_trade_no=" + payInfo.getOut_trade_no())
                .append("&sign_type=" + payInfo.getSign_type())
                .append("&spbill_create_ip=" + payInfo.getSpbill_create_ip())
                .append("&time_expire=" + payInfo.getTime_expire())
                .append("&time_start=" + payInfo.getTime_start())
                .append("&total_fee=" + payInfo.getTotal_fee())
                .append("&trade_type=" + payInfo.getTrade_type())
                .append("&key=" + appKey);

        return CommonUtil.getMD5(sb.toString().trim()).toUpperCase();
    }
}


