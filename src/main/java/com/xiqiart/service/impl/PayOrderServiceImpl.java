package com.xiqiart.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xiqiart.common.*;
import com.xiqiart.dao.OrderDao;
import com.xiqiart.entity.Order;
import com.xiqiart.entity.OrderRoom;
import com.xiqiart.entity.User;
import com.xiqiart.service.OrderService;
import com.xiqiart.service.PayOrderService;
import com.xiqiart.service.UserService;
import com.xiqiart.web.model.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class PayOrderServiceImpl implements PayOrderService {


    @Value("${shop_id}")
    private String shopId;

    @Value("${url_unified_order}")
    private String urlUnifiedOrder;

    @Value("${app_id}")
    private String appId;

    @Value("${app_secret}")
    private String appSecret;

    @Value("${app_key}")
    private String appKey;

    @Value("${time_format}")
    private String timeFormat;

    @Value("${url_notify}")
    private String urlNotify;


    @Autowired
    private OrderDao orderDao;

    @Autowired
    private UserService userService;

    @Override
    public String payOrder(PayOrderRequest payOrderRequest, HttpServletRequest request) {
        String content = null;
        UnifiedOrderResponse response = new UnifiedOrderResponse();
        Map map = new HashMap();
        ObjectMapper mapper = new ObjectMapper();

        boolean result = true;
        String info = "";


        String openId = payOrderRequest.getOpenId();
        if (StringUtils.isBlank(openId)) {
            result = false;
            info = "获取到openId为空";
        } else {
            openId = openId.replace("\"", "").trim();

            User user = userService.queryUserByUserId(payOrderRequest.getUserId());
            Order order = orderDao.orderQuery(user.getUserId(), payOrderRequest.getOrderNo());
            if(!user.getWechatOpenId().equals(openId) || order == null){
                result =false;
            }



            String clientIP = CommonUtil.getClientIp(request);
            String randomNonceStr = RandomUtils.generateMixString(32);
            response = unifiedOrder(order.getOrderNo().replace("-","")
                    ,order.getAmount(), openId, clientIP, randomNonceStr);

            if (StringUtils.isBlank(response.getPrepayId())) {
                result = false;
                info = "出错了，未获取到prepayId";
            } else {
                map.put("prepayId", response.getPrepayId());
                map.put("nonceStr", response.getNonceString());
            }
        }

        try {
            String timeStamp = getTimeStamp();
            map.put("result", result);
            map.put("info", info);
            String paySign = getPaySign(response.getNonceString(), "prepay_id=" + response.getPrepayId(), timeStamp);
            map.put("timeStamp", timeStamp);
            map.put("paySign", paySign);

            content = mapper.writeValueAsString(map);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return content;
    }

    @Override
    public int notifyOrderPaid(NotifyOrderPaidRequest request) {
        Order order = orderDao.orderQuery(request.getUserId(), request.getOrderNo());
        if(order.getOrderStatus() >= 2){
            return 0;
        }else{
            order.setLastEditTime(new Date());
            order.setOrderStatus(2);
            return orderDao.updateOrder(order);
        }
    }

    private String getTimeStamp() {
        return String.valueOf(System.currentTimeMillis() / 1000);
    }

//    private String getOpenId(String code) {
//
//        String url = "https://api.weixin.qq.com/sns/jscode2session";
//        Map<String, String> param = new HashMap<>();
//        param.put("appid", appId);
//        param.put("secret", appSecret);
//        param.put("js_code", code);
//        param.put("grant_type", "authorization_code");
//
//        String wxResult = HttpClientUtil.doGet(url, param);
//
//        WeChatSessionModel model = JsonUtils.jsonToPojo(wxResult, WeChatSessionModel.class);
//
//        return model.getOpenid();
//    }

    private UnifiedOrderResponse unifiedOrder(String orderNo, float amount, String openId, String clientIP, String randomNonceStr) {

        UnifiedOrderResponse response = new UnifiedOrderResponse();
        try {

            String url = urlUnifiedOrder;

            PayInfo payInfo = createPayInfo(orderNo, amount, openId, clientIP, randomNonceStr);
            String md5 = getSign(payInfo);
            payInfo.setSign(md5);

            String xml = CommonUtil.payInfoToXML(payInfo);
            xml = xml.replace("__", "_").replace("<![CDATA[1]]>", "1");
            //xml = xml.replace("__", "_").replace("<![CDATA[", "").replace("]]>", "");

            StringBuffer buffer = HttpUtil.httpsRequest(url, "POST", xml);

            Map<String, String> result = CommonUtil.parseXml(buffer.toString());


            String return_code = result.get("return_code");
            if (StringUtils.isNotBlank(return_code) && return_code.equals("SUCCESS")) {

                String return_msg = result.get("return_msg");
                if (StringUtils.isNotBlank(return_msg) && !return_msg.equals("OK")) {
                    //log.error("统一下单错误！");
                    return response;
                }
                response.setPrepayId(result.get("prepay_id"));
                response.setNonceString(result.get("nonce_str"));
                return response;

            } else {
                return response;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private PayInfo createPayInfo(String orderNo, float amount, String openId, String clientIP, String randomNonceStr) {

        Date date = new Date();
        String timeStart = TimeUtils.getFormatTime(date, timeFormat);
        String timeExpire = TimeUtils.getFormatTime(TimeUtils.addDay(date, 2), timeFormat);

        PayInfo payInfo = new PayInfo();
        payInfo.setAppid(appId);
        payInfo.setMch_id(shopId);
        payInfo.setDevice_info("WEB");
        payInfo.setNonce_str(randomNonceStr);
        payInfo.setSign_type("MD5");  //默认即为MD5
        payInfo.setBody("orderNo=" + orderNo);
        payInfo.setAttach("orderNo=" + orderNo);
        payInfo.setOut_trade_no(orderNo);
        payInfo.setTotal_fee((int) (amount * 100));
        payInfo.setSpbill_create_ip(clientIP);
        payInfo.setTime_start(timeStart);
        payInfo.setTime_expire(timeExpire);
        payInfo.setNotify_url(urlNotify);
        payInfo.setTrade_type("JSAPI");
        payInfo.setLimit_pay("no_credit");
        payInfo.setOpenid(openId);

        return payInfo;
    }

    private String getSign(PayInfo payInfo) throws Exception {
        StringBuffer sb = new StringBuffer();
        sb.append("appid=" + payInfo.getAppid())
                .append("&attach=" + payInfo.getAttach())
                .append("&body=" + payInfo.getBody())
                .append("&device_info=" + payInfo.getDevice_info())
                .append("&limit_pay=" + payInfo.getLimit_pay())
                .append("&mch_id=" + payInfo.getMch_id())
                .append("&nonce_str=" + payInfo.getNonce_str())
                .append("&notify_url=" + payInfo.getNotify_url())
                .append("&openid=" + payInfo.getOpenid())
                .append("&out_trade_no=" + payInfo.getOut_trade_no())
                .append("&sign_type=" + payInfo.getSign_type())
                .append("&spbill_create_ip=" + payInfo.getSpbill_create_ip())
                .append("&time_expire=" + payInfo.getTime_expire())
                .append("&time_start=" + payInfo.getTime_start())
                .append("&total_fee=" + payInfo.getTotal_fee())
                .append("&trade_type=" + payInfo.getTrade_type())
                .append("&key=" + appKey);


        return CommonUtil.getMD5(sb.toString().trim()).toUpperCase();
    }

    private String getPaySign(String nonceStr, String packageStr, String timeStamp) throws Exception {

        String payDataA = "appId=" + appId + "&nonceStr=" + nonceStr +
                "&package=" + packageStr + "&signType=MD5&timeStamp=" + timeStamp;
        String payDataB = payDataA + "&key=" + appKey;

        return CommonUtil.getMD5(payDataB).toUpperCase();
    }
}


