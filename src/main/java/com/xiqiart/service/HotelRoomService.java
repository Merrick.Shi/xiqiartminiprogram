package com.xiqiart.service;

import com.xiqiart.entity.HotelRoom;
import com.xiqiart.web.model.QueryHotelRoomResponse;

import java.util.List;

public interface HotelRoomService {

    List<HotelRoom> queryHotelRooms();
    HotelRoom queryHotelRoomById(String roomId);

    QueryHotelRoomResponse queryHotelRoomList(int pageIndex, int pageSize);

    void saveHotelRoom(HotelRoom hotelRoom);

    void updateHotelRoom(HotelRoom hotelRoom);
}
