package com.xiqiart.service;

import com.xiqiart.entity.User;
import com.xiqiart.web.model.WeChatLoginRequest;
import com.xiqiart.web.model.WeChatSessionModel;

import java.util.List;

public interface UserService {

    List<User> queryUser();

    User queryUserByUserId(String userId);

    boolean insertUser(User user);

    boolean updateUser(User user);

    boolean deleteUser(Integer userId);

    WeChatSessionModel wechatLogin(WeChatLoginRequest request);
}
