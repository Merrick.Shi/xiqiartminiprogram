package com.xiqiart.service;

import com.xiqiart.entity.Order;
import com.xiqiart.web.model.*;

import java.util.List;

public interface OrderService {

    List<Order> queryUserOrders(String userId);
    OrderDetailModel orderQuery(String userId, String orderNo);
    String placeOrder(PlaceOrderRequest request);
    List<Order> orderTabQuery(String userId, int orderStatus);
    QueryOrderResponse queryOrderList(String contactPhone, String orderNo, int pageIndex, int pageSize);
    OrderFullInfo queryOrderFullInfo(String orderNo);
    void checkInOrder(String orderNo);
    List<OrderRoomInfo> queryAllOrders();
}