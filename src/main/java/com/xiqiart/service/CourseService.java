package com.xiqiart.service;

import com.xiqiart.entity.Course;
import com.xiqiart.web.model.GroupCourse;
import com.xiqiart.web.model.QueryCourseResponse;

import java.util.List;

public interface CourseService {

    List<GroupCourse> queryCourses();
    Course queryCourseByCourseId(String courseId);

    QueryCourseResponse queryCourseList(int pageIndex, int pageSize);

    void saveCourse(Course course);
    void updateCourse(Course course);

    void deleteCourse(String courseId);
}
