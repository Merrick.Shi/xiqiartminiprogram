CREATE DATABASE IF NOT EXISTS xiqiart DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
USE xiqiart;
CREATE TABLE IF NOT EXISTS tb_user(
    id INT NOT NULL AUTO_INCREMENT,
	user_id VARCHAR(50) NOT NULL,
    user_name NVARCHAR(50),
	avatar_url NVARCHAR(500),
	wechat_open_id NVARCHAR(50),
	phone VARCHAR(20),
    create_time DATE,
    last_edit_time DATE,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS tb_course(
    id INT NOT NULL AUTO_INCREMENT,
	course_id VARCHAR(50) NOT NULL,
    course_name NVARCHAR(50),
	course_url NVARCHAR(500),
	price_org FLOAT,
	price_person FLOAT,
	month INT,
	year INT,
    create_time DATE,
    last_edit_time DATE,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS tb_hotel_room(
    id INT NOT NULL AUTO_INCREMENT,
	room_id VARCHAR(50) NOT NULL,
    room_type_name NVARCHAR(50),
	amount FLOAT NOT NULL,
	person_hold INT NOT NULL,
    create_time DATE,
    last_edit_time DATE,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS tb_order(
    id INT NOT NULL AUTO_INCREMENT,
	order_no NVARCHAR(50) NOT NULL,
	user_id NVARCHAR(50) NOT NULL,
	course_id NVARCHAR(50) NOT NULL,
    org_name NVARCHAR(50),
	attendance_count INT,
	course_name NVARCHAR(50) NOT NULL,
	course_count INT NOT NULL,
	contact_name NVARCHAR(50) NOT NULL,
    contact_phone NVARCHAR(20),
	arrive_date DATE NOT NULL,
	day_count INT NOT NULL,
	course_amount FLOAT NOT NULL,
	amount FLOAT NOT NULL,
	order_status INT NOT NULL,
	order_comment NVARCHAR(200) NOT NULL,
	create_time DATE,
    last_edit_time DATE,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS tb_order_room(
    id INT NOT NULL AUTO_INCREMENT,
	room_id VARCHAR(50) NOT NULL,
    room_type_name NVARCHAR(50),
	guest_name NVARCHAR(50) NOT NULL,
	order_no NVARCHAR(50) NOT NULL,
	amount FLOAT NOT NULL,
    create_time DATE,
    last_edit_time DATE,
    PRIMARY KEY (id)
);