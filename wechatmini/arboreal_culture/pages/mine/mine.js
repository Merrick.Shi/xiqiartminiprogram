// pages/mine/mine.js
import {
  user_info,
  change_photo,
  auth_public
} from '../../utils/api.js'
import * as store from '../../utils/store.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },

  bindGetUserInfo: function (e) {
    var that = this
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        app.globalData.code = res.code
        store.set('me', e.detail.userInfo)
        // 可以将 res 发送给后台解码出 unionId
        app.globalData.userInfo = e.detail.userInfo
        auth_public({
          shop_id: app.globalData.shop_id,
          code: app.globalData.code,
          name: e.detail.userInfo.nickName,
          photo: e.detail.userInfo.avatarUrl,
          sex: e.detail.userInfo.gender
        }).then((response) => {
          if (response.error_code == 1000) {
            app.globalData.userid = response.data.user_id
            store.set('openid', response.data.openid)
            store.set('user_id', response.data.user_id)
            that.setData({
              showLoad: false,
              avatar: e.detail.userInfo.avatarUrl,
              name: e.detail.userInfo.nickName
            })
            // that.user_info()
          }
        })
      }
    })
  },

  user_info() {
    var that = this
    user_info({
      user_id: app.globalData.userid
    }).then((res) => {
      if (res.error_code = 1000) {
        that.setData({
          avatar: res.data.photo,
          name: res.data.name
        })
      }
    })
  },
  
  // 我的报名
  tonotreceive(e) {
    wx.navigateTo({
      url: '/pages/mine/orderlist/orderlist?bindid=' + e.currentTarget.dataset.bindid,
    })
  },
  
  //优惠券
  tocoupon() {
    wx.navigateTo({
      url: '/pages/mine/coupon/coupon',
    })
  },

  // 我的拼团
  tomygroup() {
    wx.navigateTo({
      url: '/pages/mine/groupbuy/groupbuy',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    if (!store.get('user_id')) {
      that.setData({
        showLoad: true
      })
    }
    that.user_info()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})