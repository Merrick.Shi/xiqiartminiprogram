// pages/ss/ss.js


import {
  sel_cate,
  auth,
  tea_goods_show,
  add_shopcart,
  shop_info
} from '../../utils/api.js';
import * as store from '../../utils/store.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    constants: [], // 数据
    toView: null, // 左 => 右联动 右scroll-into-view 所需的id
    currentLeftSelect: null, // 当前左侧选择的
    eachRightItemToTop: [], // 右侧每类数据到顶部的距离（用来与 右 => 左 联动时监听右侧滚动到顶部的距离比较）
    leftToTop: 0,
    url: ''

  },




  getEachRightItemToTop: function () {
    var that = this
    var obj = {};
    var totop = 0;
    const RIGHT_BAR_HEIGHT = 30; // 右侧每一类的 bar 的高度（固定）
    const RIGHT_ITEM_HEIGHT = 100; // 右侧每个子类的高度（固定）
    obj[that.data.constants[0].cate_id] = totop // 右侧第一类肯定是到顶部的距离为 0
    for (let i = 1; i < (that.data.constants.length + 1); i++) { // 循环来计算每个子类到顶部的高度
      totop += (RIGHT_BAR_HEIGHT + that.data.constants[i - 1].goods.length * RIGHT_ITEM_HEIGHT)
      obj[that.data.constants[i] ? that.data.constants[i].cate_id : 'last'] = totop // 这个的目的是 例如有两类，最后需要 0-1 1-2 2-3 的数据，所以需要一个不存在的 'last' 项，此项即为第一类加上第二类的高度。
    }
    return obj
  },


  rightScroll: function (e) { // 监听右侧的滚动事件与 eachRightItemToTop 的循环作对比 从而判断当前可视区域为第几类，从而渲染左侧的对应类。
    const LEFT_ITEM_HEIGHT = 50
    for (let i = 0; i < this.data.constants.length; i++) {
      let left = this.data.eachRightItemToTop[this.data.constants[i].cate_id]
      let right = this.data.eachRightItemToTop[this.data.constants[i + 1] ? this.data.constants[i + 1].cate_id : 'last']
      if (e.detail.scrollTop < right && e.detail.scrollTop >= left) {
        this.setData({
          currentLeftSelect: this.data.constants[i].cate_id,
          leftToTop: LEFT_ITEM_HEIGHT * i
        })
      }
    }
  },

  jumpToSick: function (e) { // 左侧类的点击事件
    this.setData({
      toView: e.target.id || e.target.dataset.id,
      currentLeftSelect: e.currentTarget.dataset.id
    })
  },


  shop_info() {
    shop_info({
      shop_id: app.globalData.shop_id,
      user_id: store.get('user_id')
    }).then((res) => {
      console.log(res, '88')
    })
  },

  
  /**
   * 生命周期函数--监听页面加载
   */

  onLoad: function (options) {
    tea_goods_show({
      shop_id: app.globalData.shop_id
    }).then((res) => {
      this.data.constants = res.data.goods
      this.setData({
        constants: res.data.goods,
        currentLeftSelect: res.data.goods[0].cate_id,
        eachRightItemToTop: this.getEachRightItemToTop(),
        ungetInfo: app.globalData.ungetInfo
      })
    })
    this.shop_info()


    this.setData({
      url: options.url
    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})