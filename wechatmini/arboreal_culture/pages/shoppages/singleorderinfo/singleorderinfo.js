// pages/shoppages/singleorderinfo/singleorderinfo.js
import {
  goods_info,
  get_address,
  my_cash,
  new_order,
  cut_order,
  print_note,
  service_order,
  order_pay
} from '../../../utils/api.js'
import * as store from '../../../utils/store.js'
const app = getApp()

var dateTimePicker = require('../../../utils/dateTimePicker.js');


Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: ['男', '女'],
    objectArray: [
      {
        id: 0,
        name: '男'
      },
      {
        id: 1,
        name: '女'
      }
    ],
    totalMoney: 0,
    shopa: "",
    shop_phone: ''
  },

  totalPrice: function () {
    var that = this;
    var total_price = 0;
    for (var i = 0; i < that.data.goods.length; i++) {
      total_price += that.data.goods[i].count * parseFloat(that.data.goods[i].price)
      //console.log(that.data.goods[i].price,"12255")
    }
    that.setData({
      totalPrice: total_price.toFixed(2),
      totalMoney: total_price.toFixed(2)
    });
  },


  //付款
  formSubmit(e) {
   // console.log(e,"88888")
    var that = this
    var order_goods = []
    var flag = true
    if (e.detail.value.service_name == '') {
      wx.showLoading({
        title: '请填写姓名',
      })
      setTimeout(function () {
        wx.hideLoading()
      }, 1000)
    } else if (e.detail.value.service_phone == '') {
      wx.showLoading({
        title: '请填写手机号',
      })
      setTimeout(function () {
        wx.hideLoading()
      }, 1000)
    } else if (!(/^[1][3,4,5,7,8][0-9]{9}$/.test(e.detail.value.service_phone))) {
      wx.showLoading({
        title: '手机号不正确',
      })
      setTimeout(function () {
        wx.hideLoading()
      }, 1000)
    } else {
      flag = false
    }
    if (flag == false) {

      for (var i = 0; i < this.data.goods.length; i++) {
        var item = this.data.goods[i]
        order_goods.push({
          "goods_id": item.goods_id,
          "goods_num": item.num,
          "goods_cost": parseFloat(item.price),
          "product_id": item.product_id,
          "user_id": store.get('user_id'),
          "key_name": item.key_name,
          "service_name": item.service_name,
          "member_price": item.member_price,
          "goods_name": item.goods_name
        })
      }

      that.setData({
        disable: true,
      })

      var dates = e.detail.value.dates;
      var times = e.detail.value.times;
      var yytime = ""
      if (dates === null ) {
        yytime = "无"
      } else if (dates === null) {
        dates = ""
        yytime = dates 
      } else {
        yytime = dates 
      }
      service_order({
        user_id: app.globalData.userid,
        shop_id: app.globalData.shop_id,
        cash_id: 0,
        // message: that.data.message,
        goods_id: that.data.goods[0].goods_id,
        order_group_type: 0,
        goods_num: 1,
        goods_cost: that.data.totalMoney,
        service_phone: e.detail.value.service_phone,
        service_time: yytime,
        message: e.detail.value.message,
        product_id: that.data.goods[0].product_id[0],
        goods_name: that.data.goods[0].name,
        service_name: e.detail.value.service_name,
        service_num: 1,
        is_kj: 0,
        service_sex: e.detail.value.sex,
        service_address: e.detail.value.unitaddress ,
        // order_type:2
      }).then((res) => {
        if (res.error_code == 1000) {
          that.data.order_sn = res.data.order_sn
          that.order_pay()
        } else {
          wx.showLoading({
            title: '参数错误',
          })
          setTimeout(function () {
            wx.hideLoading()
            that.setData({
              disable: false
            })
          }, 2000)
        }
      })
    }
  },

  order_pay() {
    var that = this
    order_pay({
      shop_id: that.data.goods.shop_id,
      user_id: app.globalData.userid,
      order_sn: that.data.order_sn
    }).then((res) => {
      debugger
      wx.requestPayment({
        appId: res.appId,
        timeStamp: res.timeStamp,
        nonceStr: res.nonceStr,
        package: res.package,
        signType: res.signType,
        paySign: res.paySign,
        success: function (res) {
          //console.log(res,"1230")
          // var goods_pre = []
          // for (var i = 0; i < that.data.goods.length; i++) {
          //   var item = that.data.goods[i]
          //   goods_pre.push({
          //     goods_name: item.name,
          //     goods_price: item.price,
          //     goods_num: item.count,
          //     goods_cost: parseFloat(item.price) * item.count,
          //     key_name: item.key_name
          //   })
          // }
          // print_note({
          //   goods: goods_pre,
          //   pay_price: that.data.totalMoney,
          //   num: that.data.goods.length,
          //   name: that.data.editaddress.receiver,
          //   address: that.data.editaddress.address + that.data.editaddress.detail,
          //   phone: that.data.editaddress.phone,
          //   shop_name: that.data.shopa,
          //   shop_phone: that.data.shop_phone,
          //   order_type: 1,
          //   shop_id: app.globalData.shop_id,
          //   message: e.detail.value.message
          // }).then(res => {
          // })
          wx.navigateTo({
            url: '/pages/mine/orderlist/orderlist?bindid=0',
          })
        },
        fail: function (res) {
          wx.showLoading({
            title: '支付成功',
          })
          setTimeout(function () {
            wx.hideLoading()
            that.setData({
              disable: false
            })
          }, 1000)
        }
      })
    })
  },



  shop_info() {
    var that = this
    shop_info({
      shop_id: app.globalData.shop_id,
      user_id: app.globalData.userid
    }).then((res) => {
      if (res.error_code == 1000) {
        that.setData({
          shopa: res.data.name,
          shop_phone: res.data.data.cs_phone
        })
      }
    })
  },

  //选择性别
  bindPickerChange: function (e) {
   // console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      sex: e.detail.value
    })
  },

  //选择日期
  bindDateChange: function (e) {
    console.log(e)
    this.setData({
      date: e.detail.value
    })
  },

  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(options)
     var that = this
    store.set('coupon', '')
   

    wx.getStorage({
      key: 'singleorderinfo',
      success: function (res) {
        console.log(res,"555555")
        that.setData({
          goods: res.data,
        })
        that.totalPrice()
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this
    wx.getStorage({
      key: 'editaddress',
      success: function (res) {
        if (res != '') {
          that.setData({
            editaddress: res.data
          })
        }
      },
    })
    that.data.coupon = store.get('coupon')
    if (that.data.coupon != '') {
      if (that.data.coupon.cash_type == 1) {
        let total = that.data.totalPrice * that.data.coupon.cash_money / 10
        that.setData({
          discount: that.data.totalPrice * (10 - that.data.coupon.cash_money) / 10,
          totalMoney: total.toFixed(2),
          coupon_id: that.data.coupon.cash_id
        })
      } else if (that.data.coupon.cash_type == 0) {
        let total = that.data.totalPrice - that.data.coupon.cash_money
        that.setData({
          discount: that.data.coupon.cash_money,
          totalMoney: total.toFixed(2),
          coupon_id: that.data.coupon.cash_id
        })
      }
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})