// pages/shoppages/mall/mall.js
import {
  tea_goods_show
} from '../../../utils/api.js';
import * as store from '../../../utils/store.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    constants: [],
    toView: null,
    currentLeftSelect: null,
    eachRightItemToTop: [],
    leftToTop: 0,
    goods_id:""
  },

  getEachRightItemToTop: function() {
    var that = this
    var obj = {};
    var totop = 0;
    const RIGHT_BAR_HEIGHT = 30;
    const RIGHT_ITEM_HEIGHT = 160;
    obj[that.data.constants[0].cate_id] = totop
    for (let i = 1; i < (that.data.constants.length + 1); i++) {
      totop += (RIGHT_BAR_HEIGHT + that.data.constants[i - 1].goods.length * RIGHT_ITEM_HEIGHT)
      obj[that.data.constants[i] ? that.data.constants[i].cate_id : 'last'] = totop
    }
    return obj
  },

  rightScroll: function(e) {
    const LEFT_ITEM_HEIGHT = 50
    for (let i = 0; i < this.data.constants.length; i++) {
      let left = this.data.eachRightItemToTop[this.data.constants[i].cate_id]
      let right = this.data.eachRightItemToTop[this.data.constants[i + 1] ? this.data.constants[i + 1].cate_id : 'last']
      if (e.detail.scrollTop < right && e.detail.scrollTop >= left) {
        this.setData({
          currentLeftSelect: this.data.constants[i].cate_id,
          leftToTop: LEFT_ITEM_HEIGHT * i
        })
      }
    }
  },

  jumpToSick: function(e) { 
    this.setData({
      toView: e.target.id || e.target.dataset.id,
      currentLeftSelect: e.currentTarget.dataset.id
    })
  },



  single_order(e) {
    console.log(e)
    var that = this
    // wx.navigateTo({
    //   url: '/pages/shoppages/singleorderinfo/singleorderinfo?goods_id=' + e.currentTarget.dataset.goods_id
    //     + "&goods_name=" + e.currentTarget.dataset.goods_name + "&goods_price=" + e.currentTarget.dataset.goods_price,
    // })
    wx.setStorage({
      key: 'singleorderinfo',
      data: [{
        goods_id: e.currentTarget.dataset.goods_id,
        name: e.currentTarget.dataset.goods_name,
        price: e.currentTarget.dataset.goods_price,
        product_id: e.currentTarget.dataset.product_id,
        cate_name: e.currentTarget.dataset.cate_name,
        count: 1
      }],
    })

    wx.navigateTo({
      url: '/pages/shoppages/singleorderinfo/singleorderinfo',
    })
  },

  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    tea_goods_show({
      shop_id: app.globalData.shop_id,
    }).then((res) => {
      this.data.constants = res.data.goods
      this.setData({
        constants: res.data.goods,
        currentLeftSelect: res.data.goods[0].cate_id,
        eachRightItemToTop: this.getEachRightItemToTop(),
        ungetInfo: app.globalData.ungetInfo
      })
    })


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})