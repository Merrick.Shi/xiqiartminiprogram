// pages/shoppages/index/index.js

import {
  shop_info,
  get_cash,
  show_commend,
  auth_public
} from '../../../utils/api.js';
import * as store from '../../../utils/store.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
   
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    nav: [
      {
        name: '大咖评画',
        img: '/image/dkph1.png',
        link: "http://www.baidu.com"
      },
      {
        name: '在线师训',
        img: '/image/zxsx.png',
        link: "http://www.baidu.com"
      },
      {
        name: '线下师训',
        img: '/image/xpsx.png',
        link: "https://blog.csdn.net/qq_40670946/article/details/82495835"
      },
      {
        name: '栖奇木艺',
        img: '/image/xqmy.png',
        link: "https://blog.csdn.net/qq_40670946/article/details/82495835"
      }
    ]
  },
  
  //点击事件 https://blog.csdn.net/qq_40670946/article/details/82495835
  intoUrl: function (e) {
    console.log(e);

    let url = e.currentTarget.dataset.url;
    wx.navigateTo({
      url: '/pages/ss/ss?url=' + url, 

    })
  },  

  

  get_shopinfo(){
    var that = this
    shop_info({
      shop_id: app.globalData.shop_id,
      user_id: app.globalData.userid
    }).then((res) =>{
      // console.log(res,"11111")
      if(res.error_code ==1000){
        that.setData({
          shopa: res.data.name,
          imgUrls: res.data.data.bg_picture_url,
        })
        //  修改首页标题
        wx.setNavigationBarTitle({
          title: that.data.shopa
        })
      }
    })
  },


  // 搜索
  to_search_goods() {
    wx.navigateTo({
      url: '/pages/shoppages/search/search',
    })
  },

  
  // 跳转敬请期待页面

  Comingsoon(){
    wx.navigateTo({
      url: '/pages/shoppages/Comingsoon/Comingsoon',
    })
  },



  // 授权登录
  bindGetUserInfo: function (e) {
    var that = this
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        app.globalData.code = res.code
        store.set('me', e.detail.userInfo)
        // 可以将 res 发送给后台解码出 unionId
        app.globalData.userInfo = e.detail.userInfo
        auth_public({
          shop_id: app.globalData.shop_id,
          code: app.globalData.code,
          name: e.detail.userInfo.nickName,
          photo: e.detail.userInfo.avatarUrl,
          sex: e.detail.userInfo.gender
        }).then((response) => {
          if (response.error_code == 1000) {
            app.globalData.userid = response.data.user_id
            store.set('openid', response.data.openid)
            store.set('user_id', response.data.user_id)
            that.setData({
              showLoad: false,
              avatar: e.detail.userInfo.avatarUrl,
              name: e.detail.userInfo.nickName
            })
            // that.user_info()
          }
        })
      }
    })
  },

  
  bindcation(){
    wx.navigateTo({
      url: '/pages/shoppages/mall/mall',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   let that = this
    if (!store.get('user_id')) {
      that.setData({
        showLoad: true
      })
    }
    that.get_shopinfo()
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})